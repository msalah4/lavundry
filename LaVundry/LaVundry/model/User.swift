//
//  User.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/29/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    
    var fname = ""
    var lname = ""
    var email = ""
    var password = ""
    var phone = ""
    var address = ""
    var city = ""
    var streatName = ""
    var building = ""
    var apartment = ""
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fname = aDecoder.decodeObject(forKey: "fname") as! String
        lname = aDecoder.decodeObject(forKey: "lname") as! String
        email = aDecoder.decodeObject(forKey: "email") as! String
        password = aDecoder.decodeObject(forKey: "password") as! String
        phone = aDecoder.decodeObject(forKey: "phone") as! String
        address = aDecoder.decodeObject(forKey: "address") as! String
        city = aDecoder.decodeObject(forKey: "city") as! String
        streatName = aDecoder.decodeObject(forKey: "streatName") as! String
        building = aDecoder.decodeObject(forKey: "building") as! String
        apartment = aDecoder.decodeObject(forKey: "apartment") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(fname, forKey: "fname")
        aCoder.encode(lname, forKey: "lname")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(streatName, forKey: "streatName")
        aCoder.encode(building, forKey: "building")
        aCoder.encode(apartment, forKey: "apartment")
    }
    
    

}
