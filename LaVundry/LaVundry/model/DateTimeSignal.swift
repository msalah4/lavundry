//
//  DateTimeSignal.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/26/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import Signals

class DateTimeSignal: NSObject {
    
    let singleType = Signal<String>()
    let dateTimeValue = Signal<String>()

    
}
