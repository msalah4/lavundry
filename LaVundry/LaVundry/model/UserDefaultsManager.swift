//
//  UserDefaultsManager.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/29/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class UserDefaultsManager: NSObject {
    
    class func saveUser(_ user:User) {
        
        let defaults = UserDefaults.standard
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: user)
        defaults.setValue(encodedData, forKey: "User")
    }

    
    class func fetchUser() -> User? {
      
        if let data = UserDefaults.standard.data(forKey: "User"),
            let user = NSKeyedUnarchiver.unarchiveObject(with: data) as? User{
            return user
        } else {
            return nil
        }
    }
    
    
    class func saveCountry(_ country:String) {
        
        let defaults = UserDefaults.standard
        defaults.setValue(country, forKey: "country")
    }
    
    
    class func fetchCountry() -> String? {
        
        if let data = UserDefaults.standard.string(forKey: "country")  {
            return data 
        } else {
            return nil
        }
    }
    
    
    class func saveConfig(_ key:String,_ value:Bool) {
        
        let defaults = UserDefaults.standard
        defaults.setValue(value, forKey: key)
    }
    
    
    class func fetchValue(_ key: String) -> Bool {
        
        return UserDefaults.standard.bool(forKey: key)
           
        }
    
}
