//
//  LandingViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/29/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class LandingViewController: IntroViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var selectedCountryIndex = -1
    @IBOutlet weak var pickerView: UIPickerView!
    
    let dataSet = ["UAE", "Kuwait"]
    let imagesDataSet = [#imageLiteral(resourceName: "uae"), #imageLiteral(resourceName: "kuwait")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return dataSet[row]
//    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSet.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedCountryIndex = row
        UserDefaultsManager.saveCountry(dataSet[row])
        
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 80
    }
    
    // MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        
        let myView = UIView(frame: CGRect.init(x: 0, y: 0, width: 150, height: 80))
        
        myView.center = pickerView.center
        
        let myImageView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 30, height: 80))
        
        myImageView.contentMode = .scaleAspectFit
        var rowString = String()
       
        rowString = dataSet[row]
        myImageView.image = imagesDataSet[row]
        
        let myLabel = UILabel(frame: CGRect.init(x: 40, y: 0, width: 110, height: 80))
        myLabel.text = rowString
        
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        
        return myView
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        
        if selectedCountryIndex == -1 {
            UserDefaultsManager.saveCountry(dataSet[0])
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
