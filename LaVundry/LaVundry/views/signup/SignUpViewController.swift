//
//  SignUpViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/24/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class SignUpViewController: ParentViewController, UITextFieldDelegate {

    
    @IBOutlet weak var saveAddressLable: UILabel!

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var streetName: UITextField!
    @IBOutlet weak var apartment: UITextField!
    @IBOutlet weak var building: UITextField!
    @IBOutlet weak var saveAddress: UISwitch!
    
    @IBOutlet weak var countryLable: UILabel!
    
    
    @IBOutlet weak var signUp: UIButton!
    
    public var isUpdateMode:Bool?
    
    
    @IBOutlet weak var countrySelector: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaultsManager.fetchUser() != nil {
            signUp.setTitle("Update", for: .normal)
            let user = UserDefaultsManager.fetchUser()
            isUpdateMode = true
            firstName.text = user?.fname
            lastName.text = user?.lname
            password.text = user?.password
            emailAddress.text = user?.email
            mobile.text = user?.phone
            city.text = user?.city
            streetName.text = user?.streatName
            apartment.text = user?.apartment
            building.text = user?.building
            saveAddress.isOn = UserDefaultsManager.fetchValue("saveAdd")
        } else {
            self.navigationItem.leftBarButtonItem = nil
            countrySelector.isHidden = true
            countryLable.isHidden = true
            city.isHidden = true
            building.isHidden = true
            apartment.isHidden = true
            streetName.isHidden = true
            saveAddress.isHidden = true
            saveAddressLable.isHidden = true
        }
        
        if UserDefaultsManager.fetchCountry() == "UAE" {
            self.countrySelector.selectedSegmentIndex = 0
        } else {
            self.countrySelector.selectedSegmentIndex = 1
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func onSignUp(_ sender: UIButton) {
        
        if !valide() {
            return
        }
        
        let user = User()
        user.fname = firstName.text!
        user.lname = lastName.text!
        user.password = password.text!
        user.phone = mobile.text!
        user.email = emailAddress.text!
        user.city = city.text!
        user.streatName = streetName.text!
        user.building = building.text!
        user.apartment = apartment.text!
    
        UserDefaultsManager.saveUser(user)
        UserDefaultsManager.saveConfig("saveAdd", saveAddress.isOn)
        
        if self.countrySelector.selectedSegmentIndex == 0 {
            UserDefaultsManager.saveCountry("UAE")
        } else {
            UserDefaultsManager.saveCountry("Kuwait")
        }
        
        let base_nav = self.storyboard?.instantiateViewController(withIdentifier: "nav_quick")
        
        self.present(base_nav!, animated: true, completion: nil)
    }
    
    func valide()-> Bool {
        
        var result = true
        // check for all fields 
        
        if UserDefaultsManager.fetchUser() != nil {
            if (firstName.text?.isEmpty)! ||
                (lastName.text?.isEmpty)! ||
                (password.text?.isEmpty)! ||
                (mobile.text?.isEmpty)! ||
                (emailAddress.text?.isEmpty)! ||
                (streetName.text?.isEmpty)! ||
                (building.text?.isEmpty)! ||
                (apartment.text?.isEmpty)! ||
                (city.text?.isEmpty)! {
                showErrorMessage("Please fill all fields")
                return false
            }
        } else {
            
            if (firstName.text?.isEmpty)! ||
                (lastName.text?.isEmpty)! ||
                (password.text?.isEmpty)! ||
                (mobile.text?.isEmpty)! ||
                (emailAddress.text?.isEmpty)! {
                showErrorMessage("Please fill all fields")
                return false
            }
        }
        
        
        return result
    }
    
    
    
    func showErrorMessage(_ message:String)  {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
