//
//  AddressSelectionViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 8/3/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class AddressSelectionViewController: UIViewController {
    
    
    @IBOutlet weak var building: UITextField!
    @IBOutlet weak var apartmnt: UITextField!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var city: UITextField!
    
    @IBOutlet weak var isSave: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onDonePressed(_ sender: Any) {
        
        if !valide() {
            return
        }
        
        let user = UserDefaultsManager.fetchUser()
        user?.building = building.text!
        user?.apartment = apartmnt.text!
        user?.streatName = street.text!
        user?.city = city.text!
        
        UserDefaultsManager.saveUser(user!)
        UserDefaultsManager.saveConfig("saveAdd", isSave.isOn)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func valide()-> Bool {
        
        var result = true
        // check for all fields
        if (building.text?.isEmpty)! ||
            (apartmnt.text?.isEmpty)! ||
            (street.text?.isEmpty)! ||
            (city.text?.isEmpty)! {
            showErrorMessage("Please fill all fields")
            return false
        }
        return result
    }
    
    func showErrorMessage(_ message:String)  {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
