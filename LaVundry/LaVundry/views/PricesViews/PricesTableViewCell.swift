//
//  PricesTableViewCell.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/27/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class PricesTableViewCell: UITableViewCell {

    @IBOutlet weak var firstLabel: UILabel!
    
    @IBOutlet weak var secondLable: UILabel!
    
    @IBOutlet weak var thirdLable: UILabel!
    
    var isHeader = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setAsHeader() {

            firstLabel.textColor = UIColor.red
            secondLable.textColor = UIColor.red
            thirdLable.textColor = UIColor.red
        
    }
    
    class func instanceFromNib() -> PricesTableViewCell{
        return UINib(nibName: "PricesTableViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PricesTableViewCell
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
