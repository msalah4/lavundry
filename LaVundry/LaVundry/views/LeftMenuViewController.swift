//
//  LeftMenuViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/25/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    let SHOW_RATIO:CGFloat  = 0.75
    let CELL_HEIGHT:CGFloat  = 44
    let CELL_SETTINGS_HEIGHT:CGFloat  = 126
    let CELL_ID = "Cell"
    let CELL_SETTINGS_ID = "SETTINGS_Cell"

    let menu = ["Home", "Account", "Promotions", "Share", "Help", "Pricing"]
    let menu_icones = ["home-button", "user", "discount", "share", "info", "list"]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        
        let cellNib = UINib(nibName: "LeftMenuTableViewCell", bundle: nil)
        
        tableView.register(cellNib, forCellReuseIdentifier: CELL_ID)
        tableView.tableFooterView = UIView()
        custamizeMenu()
    
    }
    
    func custamizeMenu() {
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view, forMenu:.right)
        
        //        SideMenuManager.menuWidth = 310.67
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuPresentMode = .menuSlideIn
    }
    
    @IBAction func toggleMenu(_ sender: Any) {
        SideMenuManager.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 6;
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        var cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID) as! LeftMenuTableViewCell
        
        
        cell.cellTitle.text = menu[indexPath.row]
        cell.backgroundColor = UIColor.clear
        cell.cellImage.image = UIImage.init(named: menu_icones[indexPath.row])
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var viewController:UIViewController?
        if indexPath.row == 0 {
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "quick")
        } else if indexPath.row == 1 {
            let v = self.storyboard?.instantiateViewController(withIdentifier: "signup") as! SignUpViewController
            v.isUpdateMode = true
            viewController = v
        } else if indexPath.row == 2 {
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "discounts")
        } else if indexPath.row == 3 {
            // Share
            let navigationController = SideMenuManager.centerNavigationController
            //Set the default sharing message.
            let message = "Check this app"
            toggleMenu(UIButton.init())
            //Set the link to share.
            if let link = NSURL(string: "http://yoururl.com")
            {
                let objectsToShare = [message,link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                navigationController?.viewControllers[0].present(activityVC, animated: true, completion: nil)
            }
            
            return
            
        }  else if indexPath.row == 4 {
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "contact")
        }
        else if indexPath.row == 5 {
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "price")
        } 
        
        
        let navigationController = SideMenuManager.centerNavigationController
        navigationController?.setViewControllers([viewController!], animated: true)
        toggleMenu(UIButton.init())
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
