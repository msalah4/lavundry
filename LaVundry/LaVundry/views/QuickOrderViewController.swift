//
//  QuickOrderViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/26/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class QuickOrderViewController: UIViewController {

    @IBOutlet weak var stackContainerView: UIStackView!
    
    @IBOutlet weak var dateTimePickerView: UIDatePicker!
    
    
    @IBOutlet weak var quickOrder: UIButton!
    
    @IBOutlet weak var addressChange: UIButton!
    
    
    @IBOutlet weak var datePickerContainer: UIView!
    
    var pickerType:String?
    
    let dateTimeSignal = DateTimeSignal()
    
    var pickupView:ScheduleView?
    var deliveryView:ScheduleView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SideMenuManager.centerNavigationController = self.navigationController
        
        datePickerContainer.isHidden = true
        // Do any additional setup after loading the view.
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(QuickOrderViewController.dismissPicker))
        
        
        if !UserDefaultsManager.fetchValue("saveAdd") {
            addressChange.isHidden = true
        }
        
        quickOrder.setTitle("Next", for: .normal)
        
        
        
        
    }

    override func viewDidLayoutSubviews() {
        
        if stackContainerView.arrangedSubviews.count == 0 {
            pickupView = ScheduleView.instanceFromNib()
            deliveryView = ScheduleView.instanceFromNib()
            pickupView?.dateTimeSignal = dateTimeSignal
            deliveryView?.dateTimeSignal = dateTimeSignal
            pickupView?.applyPickUpTheme()
            
            stackContainerView.addArrangedSubview(pickupView!)
            stackContainerView.addArrangedSubview(deliveryView!)
        }
        
        dateTimeSignal.singleType.subscribe(on: self) { (type) in
            //            if type == "time" {
            //                self.dateTimePickerView.isHidden = false
            //            } else {
            //
            //            }
            self.pickerType = type
            
            if type == "time" {
                let gregorian = Calendar(identifier: .gregorian)
                let now = Date()
                var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
                
                // Change the time to 9:30:00 in your locale
                components.hour = 0
                components.minute = 0
                components.second = 0
                
                let date = gregorian.date(from: components)!
                
                self.dateTimePickerView.datePickerMode = .time
                self.dateTimePickerView.setDate(date, unit: .hour, deltaMinimum: 9, deltaMaximum: 23, animated: true)
            } else {
                self.dateTimePickerView.datePickerMode = .date
                self.dateTimePickerView.setDate(Date(), unit: .year, deltaMinimum: 0, deltaMaximum: 100, animated: true)
                self.dateTimePickerView.setDate(Date(), unit: .month, deltaMinimum: 0, deltaMaximum: -1, animated: true)
                self.dateTimePickerView.setDate(Date(), unit: .day, deltaMinimum: 0, deltaMaximum: -1, animated: true)
                
            }
            
            self.datePickerContainer.isHidden = false
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onQuickOrderClicked(_ sender: UIButton) {
        
        if pickupView?.dateText.text?.range(of: "Select") != nil ||
           deliveryView?.dateText.text?.range(of: "Select") != nil ||
            pickupView?.timeText.text?.range(of: "Select") != nil ||
            deliveryView?.timeText.text?.range(of: "Select") != nil {
            
            let alert = UIAlertController(title: "Error", message: "Please select all dates and times", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if !UserDefaultsManager.fetchValue("saveAdd") {
            let addessView = self.storyboard?.instantiateViewController(withIdentifier: "address")
            self.present(addessView!, animated: true, completion: {
                
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "inst") as! InstructionsViewController
                viewController.pickUpDate = self.pickupView?.dateText.text
                viewController.pickUpTime = self.pickupView?.timeText.text
                viewController.delivaryDate = self.deliveryView?.dateText.text
                viewController.deliveryTime = self.deliveryView?.timeText.text
                self.show(viewController, sender: nil)
            })
        } else {
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "inst") as! InstructionsViewController
            viewController.pickUpDate = pickupView?.dateText.text
            viewController.pickUpTime = pickupView?.timeText.text
            viewController.delivaryDate = deliveryView?.dateText.text
            viewController.deliveryTime = deliveryView?.timeText.text
            self.show(viewController, sender: nil)
        }
        
        
        
        
        
    }
    
    
  
    @IBAction func onDatePickerValueChanged(_ sender: UIDatePicker) {
        
       
    }
    
    @IBAction func dismissPicker () {
        
        datePickerContainer.isHidden = true
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        var str = dateFormatter.string(from: dateTimePickerView.date)
        let array = str.characters.split(separator: ",").map(String.init)
        
        if pickerType == "time" {
            str = array[1]
        } else {
            str = array[0]
        }
        
        //        str.sp
        
        dateTimeSignal.dateTimeValue.fire(str)
    }
    @IBAction func changeAddress(_ sender: UIButton) {
    
        let addessView = self.storyboard?.instantiateViewController(withIdentifier: "address")
        self.present(addessView!, animated: true, completion: {
            
    
    })
}
    



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UIDatePicker
{
    /// set the date picker values and set min/max
    /// - parameter date: Date to set the picker to
    /// - parameter unit: (years, days, months, hours, minutes...)
    /// - parameter deltaMinimum: minimum date delta in units
    /// - parameter deltaMaximum: maximum date delta in units
    /// - parameter animated: Whether or not to use animation for setting picker
    func setDate(_ date:Date, unit:NSCalendar.Unit, deltaMinimum:Int, deltaMaximum:Int, animated:Bool)
    {
        setDate(date, animated: animated)
        
        setMinMax(unit: unit, deltaMinimum: deltaMinimum, deltaMaximum: deltaMaximum)
    }
    
    /// set the min/max for the date picker (uses the pickers current date)
    /// - parameter unit: (years, days, months, hours, minutes...)
    /// - parameter deltaMinimum: minimum date delta in units
    /// - parameter deltaMaximum: maximum date delta in units
    func setMinMax(unit:NSCalendar.Unit, deltaMinimum:Int, deltaMaximum:Int)
    {
        
        
        if let gregorian = NSCalendar(calendarIdentifier:.gregorian)
        {
            if let minDate = gregorian.date(byAdding: unit, value: deltaMinimum, to: self.date)
            {
                if deltaMinimum == -1 {
                    return
                }
                minimumDate = minDate
            }
            
            if let maxDate = gregorian.date(byAdding: unit, value: deltaMaximum, to: self.date)
            {
                if deltaMaximum == -1 {
                    return
                }
                maximumDate = maxDate
            }
        }
    }
}

extension UIToolbar {
    
    func ToolbarPiker(mySelect : Selector) -> UIToolbar {
        
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
}
