//
//  DoneViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/29/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class DoneViewController: ParentViewController {

    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    var dateStr: String?
    var timeStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        date.text = dateStr
        time.text = timeStr
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleMenu(_ sender: Any) {
        SideMenuManager.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func onDonePressed(_ sender: UIButton) {
        
        let base_nav = self.storyboard?.instantiateViewController(withIdentifier: "nav_quick")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = base_nav
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
