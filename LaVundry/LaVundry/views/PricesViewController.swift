//
//  PricesViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/27/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

enum PriceDataType :Int {
    case item = 0
    case dry = 1
    case press = 2
}

class PricesViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource {

    let DESELECT_COLOR = UIColor(colorLiteralRed: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0)
    
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var menItems: UIButton!
    
    @IBOutlet weak var womenItems: UIButton!
    
    var data :NSDictionary?
    
    var dataType = "men"
    
    let CELL_ID = "cell"
    
    @IBOutlet weak var homeItems: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        let cellNib = UINib(nibName: "PricesTableViewCell", bundle: nil)
        table.register(cellNib, forCellReuseIdentifier: CELL_ID)
        
        var fileName = "items"
        
        if UserDefaultsManager.fetchCountry() != "UAE" {
            fileName = "items_k"
        }
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "plist") {
            data = NSDictionary(contentsOfFile: path)
        }
        
        print(data)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let value = (data?.value(forKey: dataType) as! Array<Any>)
        let array = (value[0] )as! Array<Any>
        return  (array ).count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:PricesTableViewCell?
        
        let cellID = "cell \(indexPath.row))"
        if let dequedcell = tableView.dequeueReusableCell(withIdentifier: cellID){
            cell! = dequedcell as! PricesTableViewCell
        } else {
            cell = PricesTableViewCell.instanceFromNib()
        }
        
        if indexPath.row == 0 {
            cell!.setAsHeader()
            
            cell!.firstLabel.text = "Items"
            cell!.secondLable.text = "Dry Clean & Press"
            cell!.thirdLable.text =  "Press Only"
            
            return cell!
        }
        
        cell!.firstLabel.text = getPriceInfo(indexPath.row - 1, .item)
        cell!.secondLable.text = getPriceInfo(indexPath.row - 1, .dry)
        cell!.thirdLable.text =  getPriceInfo(indexPath.row - 1, .press)
        
        return cell!
        
    }
    
    
    
    @IBAction func onItemsChange(_ sender: UIButton) {
        
        if sender == menItems {
            dataType = "men"
            womenItems.setTitleColor(DESELECT_COLOR, for: .normal)
            homeItems.setTitleColor(DESELECT_COLOR, for: .normal)
            menItems.setTitleColor(UIColor.white, for: .normal)
        } else if sender == womenItems {
            dataType = "women"
            womenItems.setTitleColor(UIColor.white, for: .normal)
            homeItems.setTitleColor(DESELECT_COLOR, for: .normal)
            menItems.setTitleColor(DESELECT_COLOR, for: .normal)
        } else if sender == homeItems {
            dataType = "home"
            womenItems.setTitleColor(DESELECT_COLOR, for: .normal)
            homeItems.setTitleColor(UIColor.white, for: .normal)
            menItems.setTitleColor(DESELECT_COLOR, for: .normal)
        }
        
        table.reloadData()
        
    }
    
    
    func getPriceInfo(_ index:Int,_ priceType:PriceDataType) -> String {
        
        let typeIndex = priceType.rawValue
        let array = data?.value(forKey: dataType) as! Array<Any>
        let dataArray = array[typeIndex] as! Array<String>
        
        return dataArray[index]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
