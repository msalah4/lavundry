//
//  ContactUsViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/28/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class ContactUsViewController: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onEmailClicked(_ tap:UITapGestureRecognizer) {
        let email = "customer@lavundry.com"
        if let url = NSURL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
        
        
    }
    
    @IBAction func onPhoneClicked(_ tap:UITapGestureRecognizer) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(NSURL(string: "tel://054-4080077")! as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(NSURL(string: "tel://054-4080077")! as URL as URL)
        }
        
    }
    
    @IBAction func onWhatsClicked(_ tap:UITapGestureRecognizer) {
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=+971054-4080077")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.openURL(whatsappURL!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
