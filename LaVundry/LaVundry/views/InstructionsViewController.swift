//
//  InstructionsViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/29/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire

class InstructionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var extraInstructions: UITextField!
    
    var pickUpDate:String?
    var pickUpTime:String?
    var delivaryDate:String?
    var deliveryTime:String?
    
    
    let instructionsValues = ["Flod my clothes", "Leave it at my door", "Call me before pick up", "Call me before delivery"]
    let CELL_ID = "Cell"
    
    @IBOutlet weak var isSaveConfigOn: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let cellNib = UINib(nibName: "InstructionsTableViewCell", bundle: nil)
        
        table.register(cellNib, forCellReuseIdentifier: CELL_ID)
        table.tableFooterView = UIView()
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return instructionsValues.count;
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        var cell : InstructionsTableViewCell?
        
        let cellID = "cell \(indexPath.row))"
        if let dequedcell = tableView.dequeueReusableCell(withIdentifier: cellID){
            cell! = dequedcell as! InstructionsTableViewCell
        } else {
            cell = InstructionsTableViewCell.instanceFromNib()
        }
        
        cell!.Instruction.text = instructionsValues[indexPath.row]
        cell!.isOn.isOn = UserDefaultsManager.fetchValue(instructionsValues[indexPath.row])
        
        return cell!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    
    @IBAction func onSubmit(_ sender: UIButton) {
        
        if !isInternetAvailable(){
            let alert = UIAlertController(title: "Error", message: "Please Check your internet connection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        
        if isSaveConfigOn.isOn {
            for index in 0..<instructionsValues.count {
                let cell = table.cellForRow(at: IndexPath(row: index, section: 0)) as! InstructionsTableViewCell
                UserDefaultsManager.saveConfig(instructionsValues[index], cell.isOn.isOn)
                
            }
            
            let user = UserDefaultsManager.fetchUser()
            
            var originalString = "https://lavundry.com/mobile-quick-order/?f_name=\(user?.fname ?? "")&l_name=\(user?.lname)&email=\(user?.email)&phone=\(user?.phone)&street=\(user?.streatName)&city=\(user?.city)&state=\(UserDefaultsManager.fetchCountry())&add1=\(user?.building)&add2=\(user?.apartment)&PD=\(pickUpDate)&DD=\(delivaryDate)&PT=\(pickUpTime)&DT=\(deliveryTime ?? "")&fold=\(isOn(0))&door=\(isOn(1))&Dcall=\(isOn(2))&Pcall=\(isOn(3))&inst=\(self.extraInstructions.text!)"
            var escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            Alamofire.request(escapedString!)
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "done") as! DoneViewController
            
            viewController.dateStr = delivaryDate
            viewController.timeStr = deliveryTime
            
            self.present(viewController, animated: true, completion: nil)
        }
        
    }
    
    func isOn(_ index:Int) -> String {
        return (UserDefaultsManager.fetchValue(instructionsValues[index]) ? "yes": "no")
    }
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
