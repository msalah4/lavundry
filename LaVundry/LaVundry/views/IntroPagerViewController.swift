//
//  IntroViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/24/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class IntroPagerViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    let NUMBER_OF_PAGES = 3
    var pageController: UIPageViewController?
    let images = [#imageLiteral(resourceName: "tut2"), #imageLiteral(resourceName: "tut3")]
    var currentIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaultsManager.fetchUser() != nil {
            let base_nav = self.storyboard?.instantiateViewController(withIdentifier: "nav_quick")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = base_nav
        } else {
           
            pageController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
            
            pageController!.delegate = self
            pageController!.dataSource = self
            
            let introView = self.storyboard?.instantiateViewController(withIdentifier: "landing") as! IntroViewController
            introView.index = 0
            introView.imageFile = images[0]
            
            
            let views = [introView]
            
            pageController?.setViewControllers(views, direction: .forward, animated: true, completion: nil)
            pageController!.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height);
            
            addChildViewController(pageController!)
            self.view.addSubview(pageController!.view)
            pageController!.didMove(toParentViewController: self)
        }
        
        
        

        // Do any additional setup after loading the view.
    }

  
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! IntroViewController).index
        
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index! -= 1
        
        return viewControllerAtIndex(index: index!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        var index = (viewController as! IntroViewController).index
        
        if index == NSNotFound {
            return nil
        }
        
        index! += 1
        
        if (index == NUMBER_OF_PAGES) {
            return nil
        }
        
        return viewControllerAtIndex(index: index!)
    }
    
    func viewControllerAtIndex(index: Int) -> IntroViewController?
    {
        if index >= NUMBER_OF_PAGES
        {
            return nil
        }
        
        // Create a new view controller and pass suitable data.
        var pageContentViewController = storyboard?.instantiateViewController(withIdentifier: "intro") as! IntroViewController
        
        if index == 0 {
            pageContentViewController = storyboard?.instantiateViewController(withIdentifier: "landing") as! LandingViewController
        } else {
            pageContentViewController.imageFile = images[index - 1]
        }
        
        
        pageContentViewController.index = index
        currentIndex = index
        
        return pageContentViewController
    }

//    func presentationCount(for pageViewController: UIPageViewController) -> Int {
//        return NUMBER_OF_PAGES
//    }
//   
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
