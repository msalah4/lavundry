//
//  InstructionsTableViewCell.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/29/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class InstructionsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var Instruction: UILabel!
    
    @IBOutlet weak var isOn: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    class func instanceFromNib() -> InstructionsTableViewCell{
        return UINib(nibName: "InstructionsTableViewCell", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! InstructionsTableViewCell
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
