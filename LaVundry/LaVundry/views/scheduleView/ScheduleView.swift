//
//  ScheduleView.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/25/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

enum ScheduleType {
    case Pick
    case Delivery
}

class ScheduleView: UIView {

    var dateTimeSignal:DateTimeSignal?
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var selectDate: UILabel!
    @IBOutlet weak var selectTime: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var viewIcon: UIImageView!
    var type:ScheduleType?
    
    @IBOutlet weak var dateG: UITapGestureRecognizer!
  
    @IBOutlet weak var timeG: UITapGestureRecognizer!
    
    
    class func instanceFromNib() -> ScheduleView{
        return UINib(nibName: "ScheduleView", bundle: nil).instantiate(withOwner: nil, options: nil)[3] as! ScheduleView
    }
    
    override func layoutSubviews() {
        
        dateG.addTarget(self, action: Selector(("onDateSelectClicked")))
        timeG.addTarget(self, action: Selector(("onTimeSelectClicked")))
    }
    
    func applyPickUpTheme() {
        title.text = "Pick Up"
//        selectDate.text = "Select Pick Up Date"
//        selectTime.text = "Select Pick Up Time"
//        title.textColor = UIColor.white
//        selectDate.textColor = UIColor.white
//        selectTime.textColor = UIColor.white
//        viewIcon.image = #imageLiteral(resourceName: "pickupicon")
//        self.backgroundColor = UIColor(colorLiteralRed: 113.0/255.0, green: 173.0/255.0, blue: 252.0/255.0, alpha: 1.0)
    }
    
    
    func onDateSelectClicked()  {
        dateTimeSignal!.singleType.fire("date")
        
        dateTimeSignal!.dateTimeValue.subscribe(on: self) { (value) in
            self.dateText.text = value
            self.dateTimeSignal!.dateTimeValue.cancelAllSubscriptions()
        }
        
    }

    
    func onTimeSelectClicked()  {
        dateTimeSignal!.singleType.fire("time")
        
        dateTimeSignal!.dateTimeValue.subscribe(on: self) { (value) in
            self.timeText.text = value
            self.dateTimeSignal!.dateTimeValue.cancelAllSubscriptions()
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
