//
//  ViewController.swift
//  LaVundry
//
//  Created by Mohammed Salah on 7/24/17.
//  Copyright © 2017 Msalah. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {

    
    @IBOutlet weak var img: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navButton = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(toggleSideMenu))
        navButton.tintColor = UIColor.white
        navButton.title = "menu"
        
        self.navigationItem.leftBarButtonItem = navButton
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func toggleSideMenu() {
        
        if (SideMenuManager.menuLeftNavigationController?.isBeingPresented)! {
            SideMenuManager.menuLeftNavigationController?.dismiss(animated: true, completion: nil)
        } else {
            self.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

